Source: libmarpa-r2-perl
Section: perl
Priority: optional
Build-Depends: @cdbs@, debhelper (>= 10~)
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 3.9.5
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/libmarpa-r2-perl
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-perl/packages/libmarpa-r2-perl.git
Homepage: https://metacpan.org/release/Marpa-R2

Package: libmarpa-r2-perl
Architecture: any
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Description: BNF grammar parser
 Marpa::R2 parses any language whose grammar can be written in BNF.
 That includes recursive grammars, ambiguous grammars, infinitely
 ambiguous grammars and grammars with useless or empty productions.
 Marpa does both left- and right-recursion in linear time -- in fact if
 a grammar is in any class currently in practical use, Marpa will parse
 it in linear time.
